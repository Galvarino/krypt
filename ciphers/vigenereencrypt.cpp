
#include <iostream>

using namespace std;

string message;
string mappedKey;

void messageAndKey(string msg, string key)
{
    // string msg;
    // cout << "Enter message: ";
    // getline(cin, msg);

    // message to uppercase
    for (int i = 0; i < msg.length(); i++)
    {
        msg[i] = toupper(msg[i]);
    }

    // string key;
    // cout << "Enter key: ";
    // getline(cin, key);

    // key to uppercase
    for (int i = 0; i < key.length(); i++)
    {
        key[i] = toupper(key[i]);
    }

    // mapping key to message
    string keyMap = "";
    for (int i = 0, j = 0; i < msg.length(); i++)
    {
        if (msg[i] == 32)
        {
            keyMap += 32;
        }
        else
        {
            if (j < key.length())
            {
                keyMap += key[j];
                j++;
            }
            else
            {
                j = 0;
                keyMap += key[j];
                j++;
            }
        } // if-else
    }     // for

    /*    cout << msg << "\n" << keyMap;
        cout<<"\n";
        */
    message = msg;
    mappedKey = keyMap;
}

int tableARR[26][26];
void createVigenereTable()
{
    for (int i = 0; i < 26; i++)
    {
        for (int j = 0; j < 26; j++)
        {
            int temp;
            if ((i + 65) + j > 90)
            {
                temp = ((i + 65) + j) - 26;
                tableARR[i][j] = temp;
            }
            else
            {
                temp = (i + 65) + j;
                tableARR[i][j] = temp;
            }
        }
    }
    /*   for(int i = 0; i <26; i++){
            for(int j = 0; j < 26; j++)
            {
                cout << (char)tableARR[i][j] << " ";
            }
            cout << endl;
        }*/
}

void cipherEncryption(string message, string mappedkey)
{
    createVigenereTable();
    string encrypted_text = "";
    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 32 && mappedKey[i] == 32)
        {
            encrypted_text += " ";
        }
        else
        {
            int x = (int)message[i] - 65;
            int y = (int)mappedKey[i] - 65;
            encrypted_text += (char)tableARR[x][y];
        }
    }
    cout << encrypted_text;
}

int main(int argc, char **argv)
{
    messageAndKey(argv[1], argv[2]);
    cipherEncryption(message, mappedKey);
}
