
#include <iostream>

using namespace std;

string message;
string mappedKey;

void messageAndKey(string msg, string key)
{
    // string msg = message;
    // cout << "Enter message: ";
    // getline(cin, msg);

    // message to uppercase
    for (int i = 0; i < msg.length(); i++)
    {
        msg[i] = toupper(msg[i]);
    }

    // string key;
    // cout << "Enter key: ";
    // getline(cin, key);

    // key to uppercase
    for (int i = 0; i < key.length(); i++)
    {
        key[i] = toupper(key[i]);
    }

    // mapping key to message
    string keyMap = "";
    for (int i = 0, j = 0; i < msg.length(); i++)
    {
        if (msg[i] == 32)
        {
            keyMap += 32;
        }
        else
        {
            if (j < key.length())
            {
                keyMap += key[j];
                j++;
            }
            else
            {
                j = 0;
                keyMap += key[j];
                j++;
            }
        } // if-else
    }     // for

    /*    cout << msg << "\n" << keyMap;*/
    message = msg;
    mappedKey = keyMap;
}

int itrCount(int key, int msg)
{
    int counter = 0;
    string result = "";

    for (int i = 0; i < 26; i++)
    {
        if (key + i > 90)
        {
            result += (char)(key + (i - 26));
        }
        else
        {
            result += (char)(key + i);
        }
    }
    for (int i = 0; i < result.length(); i++)
    {
        if (result[i] == msg)
        {
            break;
        }
        else
        {
            counter++;
        }
    }
    return counter;
}

void cipherDecryption(string message, string mappedKey)
{
    string decryptedText = "";
    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 32 && mappedKey[i] == 32)
        {
            decryptedText += " ";
        }
        else
        {
            int temp = itrCount((int)mappedKey[i], (int)message[i]);
            decryptedText += (char)(65 + temp);
        }
    }
    cout << decryptedText;
}

int main(int argc, char **argv)
{
    messageAndKey(argv[1], argv[2]);
    cipherDecryption(message, mappedKey);
}
