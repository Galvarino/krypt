#include <iostream>

using namespace std;

void cipherDecryption(string message)
{
    for (int i = 0; i < message.length(); i++)
    {
        message[i] = toupper(message[i]);
    }
    string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    string reversealpha = "";
    for (int i = alpha.length() - 1; i > -1; i--)
    {
        reversealpha += alpha[i];
    }
    string decrytext = "";
    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 32)
        {
            decrytext += " ";
        }
        else
        {
            for (int j = 0; j < reversealpha.length(); j++)
            {
                if (message[i] == reversealpha[j])
                {
                    decrytext += alpha[j];
                    break;
                }
            }
        }
    }
    cout << decrytext;
}

int main(int argc, char **argv)
{
    cipherDecryption(argv[1]);
}