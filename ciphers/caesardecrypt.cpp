#include <iostream>

using namespace std;

string decrypt(string message, int key)
{
    char ch;
    int i;
    for (i = 0; message[i] != '\0'; ++i)
    {
        ch = message[i];
        if (ch >= 'a' && ch <= 'z')
        {
            ch = ch - key;
            if (ch < 'a')
            {
                ch = ch + 'z' - 'a' + 1;
            }
            message[i] = ch;
        }
        else if (ch >= 'A' && ch <= 'Z')
        {
            ch = ch - key;
            if (ch > 'a')
            {
                ch = ch + 'Z' - 'A' + 1;
            }
            message[i] = ch;
        }
    }
    return message;
}

int main(int argc, char **argv)
{
    string text = argv[1];
    int s = stoi(argv[2]);
    
    cout << decrypt(text, s);
    return 0;
}