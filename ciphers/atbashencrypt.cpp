#include <iostream>

using namespace std;

void cipherEncryption(string message)
{

    // message to upper case
    for (int i = 0; i < message.length(); i++)
    {
        message[i] = toupper(message[i]);
    }

    string alpa = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    string reverseAlpa = "";
    for (int i = alpa.length() - 1; i > -1; i--)
    {
        reverseAlpa += alpa[i];
    }

    string encryText = "";
    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 32)
        {
            encryText += " ";
        }
        else
        {
            for (int j = 0; j < alpa.length(); j++)
            {
                if (message[i] == alpa[j])
                {
                    encryText += reverseAlpa[j];
                    break;
                }
            } // inner for
        }     // if-else
    }         // for

    cout << encryText;
}

int main(int argc, char **argv)
{
    cipherEncryption(argv[1]);
}