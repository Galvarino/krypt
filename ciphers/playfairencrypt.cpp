#include<algorithm>
#include<string>

#include<bits/stdc++.h>
using namespace std;

typedef struct
{
    int row;
    int col;
}position;

char mat[5][5];

void generateMatrix(string key)
{
    /* flag keeps track of letters that are filled in matrix */
	/* flag = 0 -> letter not already present in matrix */
	/* flag = 1 -> letter already present in matrix */
    
    int flag[26]={0};
    int x=0,y=0;

    for(int i=0;i<key.length();i++)
    {
        if(key[i]=='j')
        {
            key[i]='i';
        }
        if(flag[key[i]-'a']==0)
        {
            mat[x][y++]=key[i];
            flag[key[i]-'a']=1;
        }
        if(y==5)
        {
            x++;
            y=0;
        }
    }
    
    for(char ch = 'a'; ch <= 'z'; ch++)
    {
        if(ch == 'j') continue; // don't fill j since j was replaced by i

        if(flag[ch - 'a'] == 0)
        {
            mat[x][y++] = ch;
            flag[ch - 'a'] = 1 ;
        }
        if(y==5) x++, y=0;
    }
}

string formatMessage(string msg)
{
    for(int i=0;i<msg.length();i++)
    {
        if(msg[i]=='j')
        {
            msg[i]='i';
        }
    }
    msg.erase(std::remove_if(msg.begin(), msg.end(), ::isspace), msg.end());
    for(int i=1; i<msg.length(); i+=2) //pairing two characters
    {
        if(msg[i-1] == msg[i])  msg.insert(i, "x");
    }
    
    if(msg.length()%2 != 0)  msg += "x";
    return msg;
}

position getPosition(char c)
{
    for(int i=0;i<5;i++)
    {
        for(int j=0;j<5;j++)
        {
            if(c==mat[i][j])
            {
                position p={i,j};
                return p;
            }
        }
    }
}

string encrypt(string message)
{
    string ctext="";
    for(int i=0;i<message.length();i=i+2)    // i is incremented by 2 inorder to check for pair values
    {
        position p1=getPosition(message[i]);
        position p2=getPosition(message[i+1]);
        int x1=p1.row;
        int y1=p1.col;
        int x2=p2.row;
        int y2=p2.col;

         if( x1 == x2 ) // same row
        {
            ctext +=  mat[x1][(y1+1)%5];
            ctext +=  mat[x2][(y2+1)%5];
        }
        else if( y1 == y2 ) // same column
        {
            ctext += mat[ (x1+1)%5 ][ y1 ];
            ctext += mat[ (x2+1)%5 ][ y2 ];
        }
        else
        {
            ctext += mat[ x1 ][ y2 ];
            ctext += mat[ x2 ][ y1 ];
        }
    }
    return ctext;
}

int main(int argc, char **argv)
{
    // char plainText[SIZE], key[SIZE];
    //
    // strcpy(plainText, argv[1]);
    // strcpy(key, argv[2]);

    generateMatrix(argv[2]);

    string fmsg=formatMessage(argv[1]);

    string cipherText=encrypt(fmsg);

    cout<<cipherText;
}

