#include <bits/stdc++.h>
using namespace std;

map<int, int> keymap;

void setPermutationOrder(string key)
{
    for (int i = 0; i < key.length(); i++)
    {
        keymap[key[i]] = i;
    }
}

string decryptMessage(string cipher, string key)
{
    int col = key.length();
    int row = cipher.length() / col;
    char cipherMat[row][col];

    for (int j = 0, k = 0; j < col; j++)
    {
        for (int i = 0; i < row; i++)
        {
            cipherMat[i][j] = cipher[k++];
        }
    }

    int index = 0;
    for (map<int, int>::iterator ii = keymap.begin(); ii != keymap.end(); ++ii)
    {
        ii->second = index++;
    }
    char decCipher[row][col];
    map<int, int>::iterator ii = keymap.begin();
    int k = 0;
    for (int l = 0, j; key[l] != '\0'; k++)
    {
        j = keymap[key[l++]];
        for (int i = 0; i < row; i++)
        {
            decCipher[i][k] = cipherMat[i][j];
        }
    }

    string msg = "";
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (decCipher[i][j] != '_')
            {
                msg += decCipher[i][j];
            }
        }
    }
    return msg;
}

int main(int argc, char **argv)
{
    string msg = argv[1], key = argv[2];

    setPermutationOrder(key);
    string plaintext = decryptMessage(msg, key);

    cout << plaintext;
    return 0;
}