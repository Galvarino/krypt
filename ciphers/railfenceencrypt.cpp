#include <iostream>
#include <regex>

using namespace std;

void cipherEncryption(string message, int rails)
{

    // removing white space from message
    message = regex_replace(message, regex("\\s+"), "");

    // creating empty matrix
    char railMatrix[rails][message.length()];
    for (int i = 0; i < rails; i++)
    {
        for (int j = 0; j < message.length(); j++)
        {
            railMatrix[i][j] = '.';
        }
    }

    // testing newly created matrix
    //   for (int i = 0; i < rails; i++){
    //       for (int j = 0; j < message.length(); j++){
    //           cout << railMatrix[i][j];
    //      }
    //       cout << endl;
    //   }

    // Putting message letters one by one in rail matrix in zig zag

    int row = 0;
    int check = 0;
    for (int i = 0; i < message.length(); i++)
    {
        if (check == 0)
        {
            railMatrix[row][i] = message[i];
            row++;
            if (row == rails)
            {
                check = 1;
                row--;
            }
        }
        else if (check == 1)
        {
            row--;
            railMatrix[row][i] = message[i];
            if (row == 0)
            {
                check = 0;
                row = 1;
            }
        }
    }

    // Creating encrypted text

    string encryptText = "";
    for (int i = 0; i < rails; i++)
    {
        for (int j = 0; j < message.length(); j++)
        {
            /*            cout<<railMatrix[i][j];*/
            encryptText += railMatrix[i][j];
        }
        /*    cout<<endl;*/
    }

    encryptText = regex_replace(encryptText, regex("\\."), "");
    cout << encryptText;
}
int main(int argc, char **argv)
{
    cipherEncryption(argv[1], stoi(argv[2]));
}
