const { exec } = require('child_process')
const { randomFillSync } = require('crypto')
const { promisify } = require('util')

const caesarEncrypt = async (text, shift) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/caesarencrypt "${text}" "${shift}"`)
    return stdout
}

const caesarDecrypt = async (text, shift) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/caesardecrypt "${text}" "${shift}"`)
    return stdout
}

const playfairEncrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/playfairencrypt "${text}" "${key}"`)
    return stdout
}

const playfairDecrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/playfairdecrypt "${text}" "${key}"`)
    return stdout
}

const vigenereEncrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/vigenereencrypt "${text}" "${key}"`)
    return stdout
}

const vigenereDecrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/vigeneredecrypt "${text}" "${key}"`)
    return stdout
}

const atbashEncrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/atbashencrypt "${text}"`)
    return stdout
}

const atbashDecrypt = async (text) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/atbashdecrypt "${text}"`)
    return stdout
}

const ctEncrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/ctencrypt "${text}" "${key}"`)
    return stdout
}

const ctDecrypt = async (text, key) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/ctdecrypt "${text}" "${key}"`)
    return stdout
}

const railfenceEncrypt = async (text, rails) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/railfenceencrypt "${text}" "${rails}"`)
    return stdout
}

const railfenceDecrypt = async (text, rails) => {
    const { stdout } = await promisify(exec)(`${process.cwd()}/ciphers/railfencedecrypt "${text}" "${rails}"`)
    return stdout
}

module.exports = {
    caesarEncrypt,
    caesarDecrypt,
    playfairEncrypt,
    playfairDecrypt,
    vigenereEncrypt,
    vigenereDecrypt,
    atbashEncrypt,
    atbashDecrypt,
    ctEncrypt,
    ctDecrypt,
    railfenceEncrypt,
    railfenceDecrypt
}
