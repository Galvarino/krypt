# How to use?
In an HTTP GET request to `/ciphers`, pass the parameters after `/ciphers?` separated by `&`s according to the method, for example:
    
    /ciphers?method=caesar&type=decrypt&text=testing&shift=5

## caesar
    method = caesar (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)
    shift = the shift (integer)

## playfair
    method = playfair (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)
    key = the key (string)

## vigenere
    method = vigenere (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)
    key = the key (string)

## atbash
    method = atbash (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)

## ct
    method = ct (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)
    key = the key (string)

## railfence
    method = railfence (string)
    type = encrypt | decrypt (string, default: encrypt)
    text = the message (string)
    rails = the number of rails (integer)

# Project Instructions

## Setup  
    npm install

## Start
    npm start

## Development Start 
    npm run dev

## Run tests 
    npm run test
