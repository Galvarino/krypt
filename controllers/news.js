const newsRouter = require('express').Router()
require('express-async-errors')
const axios = require('axios')

const NEWSORG = process.env.NEWSORG

newsRouter.get('/', async (req, res) => {
    const news = await axios.get(NEWSORG)
    res.json(news.data.articles)
})

module.exports = newsRouter