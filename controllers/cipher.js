const cipherRouter = require('express').Router()
const ciphers = require('../ciphers/methods')
require('express-async-errors')

cipherRouter.get('/', async (req, res) => {
    const { method, type } = req.query
    if (!method) {
        return res.status(400).json({ error: 'method missing' })
    }
    if (method === 'caesar') {
        if (type === 'decrypt') {
            const { text, shift } = req.query
            if (!text || Number.isNaN(parseInt(shift))) {
                return res.status(400).json({ error: 'text or shift missing' })
            }
            res.json({ decrypted: await ciphers.caesarDecrypt(text, shift) })
        }
        else {
            const { text, shift } = req.query
            if (!text || Number.isNaN(parseInt(shift))) {
                return res.status(400).json({ error: 'text or shift missing' })
            }
            res.json({ encrypted: await ciphers.caesarEncrypt(text, shift) })
        }
    }
    else if (method === 'playfair') {
        if (type === 'decrypt') {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ decrypted: await ciphers.playfairDecrypt(text, key) })
        }
        else {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ encrypted: await ciphers.playfairEncrypt(text, key) })
        }
    }
    else if (method === 'vigenere') {
        if (type === 'decrypt') {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ decrypted: await ciphers.vigenereDecrypt(text, key) })
        }
        else {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ encrypted: await ciphers.vigenereEncrypt(text, key) })
        }
    }
    else if (method === 'atbash') {
        if (type === 'decrypt') {
            const { text } = req.query
            if (!text) {
                return res.status(400).json({ error: 'text missing' })
            }
            res.json({ decrypted: await ciphers.atbashDecrypt(text) })
        }
        else {
            const { text } = req.query
            if (!text) {
                return res.status(400).json({ error: 'text missing' })
            }
            res.json({ encrypted: await ciphers.atbashEncrypt(text) })
        }
    }
    else if (method === 'ct') {
        if (type === 'decrypt') {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ decrypted: await ciphers.ctDecrypt(text, key) })
        }
        else {
            const { text, key } = req.query
            if (!text || !key) {
                return res.status(400).json({ error: 'text or key missing' })
            }
            res.json({ encrypted: await ciphers.ctEncrypt(text, key) })
        }
    }
    else if (method === 'railfence') {
        if (type === 'decrypt') {
            const { text, rails } = req.query
            if (!text || Number.isNaN(parseInt(rails))) {
                return res.status(400).json({ error: 'text or rails missing' })
            }
            res.json({ decrypted: await ciphers.railfenceDecrypt(text, rails) })
        }
        else {
            const { text, rails } = req.query
            console.log(parseInt(rails))
            if (!text || Number.isNaN(parseInt(rails))) {
                return res.status(400).json({ error: 'text or rails missing' })
            }
            res.json({ encrypted: await ciphers.railfenceEncrypt(text, rails) })
        }
    }
    else {
        return res.status(400).json({ error: 'invalid method' })
    }
})

module.exports = cipherRouter