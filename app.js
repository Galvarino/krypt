const express = require('express')
const cipherRouter = require('./controllers/cipher')
const cors = require('cors')
const middleware = require('./utils/middleware')
const newsRouter = require('./controllers/news')
const path = require('path')
const app = express()

app.use(cors())
app.use(middleware.errorHandler)
app.use(express.static(path.join(__dirname, 'build')))
app.use('/ciphers', cipherRouter)
app.use('/news', newsRouter)
module.exports = app