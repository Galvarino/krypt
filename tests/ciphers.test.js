const supertest = require('supertest')
const app = require('../app')
const api = supertest(app)

test('spaces in the text', async () => {
    const text = "spaced text"
    const req = `/ciphers?method=caesar&type=encryt&text=${text.replace(' ', '+')}&shift=5`
    const res = await api
        .get(req)
        .expect(200)
    expect(res.body.encrypted).toBe('xufhjiYyjcy')
})

test('default type is encrypt', async () => {
    const text = "test"
    const req = `/ciphers?method=caesar&text=${text.replace(' ', '+')}&shift=5`
    const res = await api
        .get(req)
        .expect(200)
    expect(res.body.encrypted).toBe('yjxy')
})

test('missing parameters', async () => {
    const text = "test"
    const req = `/ciphers?text=${text.replace(' ', '+')}&shift=5`
    const res = await api
        .get(req)
        .expect(400)
    expect(res.body.error).toBe('method missing')
})