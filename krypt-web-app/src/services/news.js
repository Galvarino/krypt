import axios from 'axios'

const fetchNews = async () => {
    try {
        const news = await axios.get('/news')
        return news.data.map(element => { return { description: element.description, title: element.title, imgUrl: element.urlToImage } })
    }
    catch (e) {
        console.error(e)
        return [{ description: "Couldn't fetch the news", title: "Error" }]
    }
}
export { fetchNews }