import { Box, Button, Typography, Select, MenuItem, InputLabel, FormControl, TextField } from "@mui/material"
import { useState } from "react"
import axios from 'axios'

const apiBaseUrl = '/ciphers'
const feedbackUrl = process.env.REACT_APP_MAILTO

const Home = ({ user, setUser }) => {
    const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1)
    const replaceSpaces = str => str.replace(' ', '+')

    const [encode, setEncode] = useState(true)
    const [method, setMethod] = useState('')
    const [text, setText] = useState('')
    const [keyText, setKeyText] = useState('')
    const [keyValue, setKeyValue] = useState('')
    const [fetched, setFetched] = useState('')

    const signoutHandler = () => {
        setUser(undefined)
        window.localStorage.removeItem('user')
    }
    const methodHandler = (e) => {
        setMethod(e.target.value)
        switch (e.target.value) {
            case 'caesar':
                setKeyText('shift')
                break;
            case 'atbash':
                setKeyText('')
                break;
            case 'railfence':
                setKeyText('rails')
                break;
            default:
                setKeyText('key')
                break;
        }
    }
    const textHandler = (e) => {
        setText(e.target.value)
    }
    const keyValueHandler = (e) => {
        setKeyValue(e.target.value)
    }
    const submitHandler = async (e) => {
        try {
            const type = encode ? 'encrypt' : 'decrypt'
            let url = `${apiBaseUrl}?method=${method}&type=${type}&text=${replaceSpaces(text)}`
            if (keyText) {
                url = url + `&${keyText}=${replaceSpaces(keyValue)}`
            }
            const res = await axios.get(url)
            encode ? setFetched(res.data.encrypted) : setFetched(res.data.decrypted)
        }
        catch (e) {
            console.error(e)
            setFetched('Error!')
        }
    }
    const feedbackHandler = () => {
        window.location.href = feedbackUrl
    }
    return (
        <Box>
            <Box
                display='flex'
                flexDirection='row'
                justifyContent='space-between'
                alignItems='center'
                width='100%'
                padding='20px 40px'
                boxSizing='border-box'
                height='15vh'
            >
                <Typography variant="h3" fontWeight='700'>KRYPT.</Typography>
                <Box>
                    <Button onClick={feedbackHandler} size='large' sx={{ color: 'black', marginRight: '40px' }}>
                        Feedback
                    </Button>
                    <Button
                        variant='contained'
                        sx={{
                            margin: '20px 0',
                            background: 'black',
                            color: 'white'
                        }}
                        onClick={signoutHandler} >
                        Sign out
                    </Button>
                </Box>
            </Box >
            <Box
                boxSizing='border-box'
                display='flex'
                justifyContent='space-between'
                flexDirection='column'
                alignItems='center'
                width='100%'
                sx={{
                    padding: '0 40px 40px 40px',
                    height: '85vh'
                }}>
                <Box width='100%'>
                    <Typography display='inline'>Welcome </Typography>
                    <Typography display='inline' fontWeight='700'>{user.email.split('@')[0]}!</Typography>
                </Box>
                <Box
                    display='flex'
                    width='60%'
                    height='70vh'
                    flexDirection='column'
                    alignItems='center'
                    justifyContent='space-evenly'
                    padding='20px'
                    paddingTop='50px'
                    boxSizing='border-box'
                >
                    {/* <Typography variant="h3" fontWeight='600' paddingBottom='20px'>Ciphers</Typography> */}
                    <Box
                        display='flex'
                        flexDirection='row'
                        border='2px solid black'
                        borderRadius='5px'
                        width='100%'
                        boxSizing='border-box'
                    >
                        <Typography
                            padding='15px'
                            textAlign='center'
                            flexGrow='1'
                            sx={{
                                background: encode ? 'black' : 'white',
                                color: encode ? 'white' : 'black',
                                cursor: 'pointer'
                            }}
                            onClick={() => setEncode(true)}
                        >
                            Encode
                        </Typography>
                        <Typography
                            padding='15px'
                            textAlign='center'
                            flexGrow='1'
                            sx={{
                                background: encode ? 'white' : 'black',
                                color: encode ? 'black' : 'white',
                                cursor: 'pointer'
                            }}
                            onClick={() => setEncode(false)}
                        >
                            Decode
                        </Typography>
                    </Box>
                    <Box display='flex' width='100%'>
                        <FormControl sx={{ marginRight: '5px', width: '100%', flexGrow: 1 }}>
                            <InputLabel id="method">Method</InputLabel>
                            <Select
                                labelId="method"
                                value={method}
                                label="Method"
                                onChange={methodHandler}
                                required={true}
                            >
                                <MenuItem value='caesar'>Caesar</MenuItem>
                                <MenuItem value='playfair'>Playfair</MenuItem>
                                <MenuItem value='vigenere'>Vigenere</MenuItem>
                                <MenuItem value='atbash'>AtBash</MenuItem>
                                <MenuItem value='ct'>Columnar Transposition</MenuItem>
                                <MenuItem value='railfence'>Railfence</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl sx={{ marginLeft: '5px', width: '100%', flexGrow: 1 }}>
                            <TextField value={keyValue} onChange={keyValueHandler} variant='outlined' label={keyText === '' ? 'Disabled' : capitalize(keyText)} disabled={keyText === '' ? true : false} width='204px'></TextField>
                        </FormControl>
                    </Box>
                    <FormControl sx={{ width: '100%' }}>
                        <TextField value={text} onChange={textHandler} variant='outlined' label={encode ? 'Plain Text' : 'Encrypted Text'} width='204px'></TextField>
                    </FormControl>
                    <Button variant="contained"
                        sx={{
                            width: '100%',
                            boxShadow: 'none',
                            background: 'black',
                            padding: '0.8em',
                            fontSize: '1.1rem'
                        }}
                        onClick={submitHandler}
                    >
                        {encode ? 'Encrypt' : 'Decrypt'}
                    </Button>
                    <FormControl sx={{ width: '100%' }}>
                        <TextField disabled={fetched && fetched !== 'Error!' ? false : true} value={fetched} variant='outlined' label={encode ? 'Encrypted Text' : 'Decrypted Text'} width='204px'></TextField>
                    </FormControl>
                </Box>
            </Box>
        </Box >
    )
}

export default Home