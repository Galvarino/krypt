import { Box, Typography, TextField, Button } from "@mui/material"
import { auth } from '../firebase'
import { createUserWithEmailAndPassword } from 'firebase/auth'
import { useState } from "react"

const SignupForm = ({ setSignup, setUser, displayError }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const emailHandler = (e) => {
        setEmail(e.target.value)
    }
    const passwordHandler = (e) => {
        setPassword(e.target.value)
    }
    const signupHandler = async () => {
        try {
            const userCredentials = await createUserWithEmailAndPassword(auth, email, password)
            console.log(userCredentials._tokenResponse.email)
            setUser({ email: userCredentials._tokenResponse.email })
            window.localStorage.setItem('user', JSON.stringify({ email: userCredentials._tokenResponse.email }))
        }
        catch (e) {
            displayError(e.message)
        }
    }
    return (
        <Box
            display='flex'
            flexDirection='column'
            justifyContent='space-between'
            minHeight='450px'
            padding='40px'
            paddingBottom={{ xs: '20px', md: '40px' }}
            boxSizing='border-box'
            width={{ xs: '100%', md: '50%' }}
        >
            <Typography
                variant="h2"
                color="white"
                width='100%'
                fontWeight='600'
                letterSpacing='3px'
            >
                KRYPT.
            </Typography>
            <Box>
                <Typography variant="h4" color='lightgrey' position='relative' left='-2px'>Signup</Typography>
                <Typography color="lightgrey" paddingTop='10px' display={{ xs: '' }}>Welcome! Please enter your details</Typography>

            </Box>

            <TextField label="Email" value={email} onChange={emailHandler} variant="outlined" fullWidth={true} size='medium' sx={{ display: { xs: "none", md: "block" } }} />
            <TextField label="Email" value={email} onChange={emailHandler} variant="outlined" fullWidth={true} size='small' sx={{ display: { xs: "block", md: "none" } }} />
            <TextField label="Password" value={password} onChange={passwordHandler} variant="outlined" type="password" fullWidth={true} size='medium' sx={{ display: { xs: "none", md: "block" } }} />
            <TextField label="Password" value={password} onChange={passwordHandler} variant="outlined" type="password" fullWidth={true} size='small' sx={{ display: { xs: "block", md: "none" } }} />

            <Button
                variant="contained"
                sx={{
                    display: 'block',
                    width: '100%',
                    backgroundColor: 'white',
                    color: 'black'
                }}
                onClick={signupHandler}
            >
                Sign in
            </Button>
            <Box>
                <Typography variant="body1" color="lightgrey" display='inline'>Have an account? </Typography>
                <Typography variant="body1" color="crimson" display='inline' sx={{ fontWeight: 700, cursor: 'pointer' }} onClick={() => setSignup(false)}>
                    Login instead!
                </Typography>
            </Box>
        </Box>
    )
}

export default SignupForm