import { Box, Snackbar, Alert } from '@mui/material'
import background from './florian-olivo-4hbJ-eymZ1o-unsplash.jpg'
import LoginForm from './LoginForm'
import News from './News'
import { useState } from 'react'
import SignupForm from './SignupForm'

const Login = ({ setUser }) => {
    const [signup, setSignup] = useState(false)
    const [open, setOpen] = useState(false)
    const [errorMessage, setErrorMessage] = useState('no errors')

    const displayError = (error) => {
        setErrorMessage(error)
        setOpen(true)
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    }

    return (
        <Box
            display='flex'
            justifyContent='center'
            alignItems='center'
            height='100vh'
            width='100vw'
            sx={{
                background: `url(${background})`,
                backgroundSize: 'cover',
                overflow: 'auto'
            }}
        >
            <Box
                display='flex'
                flexDirection={{ xs: 'column', md: 'row' }}
                justifyContent='space-between'
                minHeight='500px'
                sx={{
                    borderRadius: '20px',
                    background: '#00000055',
                    border: '0.5mm solid #ffffff77',
                    backdropFilter: 'blur(20px)',
                    height: '75vh',
                    width: '70vw',
                    overflow: 'clip'
                }}
            >
                {signup
                    ? (<SignupForm setSignup={setSignup} setUser={setUser} displayError={displayError} />)
                    : (<LoginForm setSignup={setSignup} setUser={setUser} displayError={displayError} />)
                }

                <News />
            </Box>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </Box >
    )
}

export default Login