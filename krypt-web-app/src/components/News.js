import { Box, Typography } from '@mui/material'
import { fetchNews } from '../services/news'
import { useEffect, useState } from 'react'
import NavigateNextIcon from '@mui/icons-material/NavigateNext'
import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore'

const News = () => {
    const [news, setNews] = useState(undefined)
    const [currIndex, setCurrIndex] = useState(0)

    const getNews = async () => {
        const temp = await fetchNews()
        setNews(temp)
    }
    
    useEffect(() => {
        getNews()
    }, [])

    return (
        <Box
            display='flex'
            justifyContent='center'
            alignItems='center'
            width={{ xs: '100%', md: '50%' }}
            height={{ xs: '20%', md: '100%' }}
            padding='20px'
            boxSizing='border-box'
            color='lightgrey'
            sx={{
                background: '#ffffff33',
            }}
        >
            <NavigateBeforeIcon
                onClick={() => {
                    currIndex === 0
                        ? setCurrIndex(news.length - 1)
                        : setCurrIndex(currIndex - 1)
                }
                }
                sx={{
                    cursor: 'pointer'
                }} />
            {news
                ?
                <Box width='100%' sx={{ padding: '10px 30px 0 30px' }}>
                    {news[currIndex].description !== 'Error'
                        ? (<Typography variant='h3' marginBottom='20px' fontWeight='700' display={{ xs: 'none', md: 'block' }}>News</Typography>)
                        : null}
                    <Box display='flex' flexDirection='row'
                        alignItems='center'
                        sx={{
                            marginBottom: '30px'
                        }}>
                        {/* {news[currIndex].description !== 'Error' ? images[currIndex] : null} */}
                        <Typography display='inline' sx={{
                            typography: { xs: 'body1', md: 'h4' },
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            display: '-webkit-box',
                            WebkitLineClamp: '3',
                            WebkitBoxOrient: 'vertical'
                        }}>{news[currIndex].title}</Typography>
                    </Box>
                    <Typography variant='body1' display={{ xs: 'none', md: 'inline' }}>{news[currIndex].description}</Typography>
                </Box>
                : <Typography variant='body1'>Fetching...</Typography>}
            <NavigateNextIcon
                onClick={() => {
                    setCurrIndex((currIndex + 1) % news.length)
                }}
                sx={{
                    cursor: 'pointer'
                }}
            />
        </Box >
    )
}

export default News