import { useEffect, useState } from "react"
import Login from "./components/Login"
import { ThemeProvider, createTheme } from '@mui/material/styles'
import Home from "./components/Home"

const darkTheme = createTheme({
  palette: {
    mode: 'dark'
  },
  typography: {
    fontFamily: [
      'JetBrains Mono',
      'monospace'
    ]
  }
})

const lightTheme = createTheme({
  palette: {
    mode: 'light'
  },
  typography: {
    fontFamily: [
      'JetBrains Mono',
      'monospace'
    ]
  }
})

const App = () => {
  const [user, setUser] = useState(undefined)

  useEffect(() => {
    const loggedInUser = window.localStorage.getItem('user')
    if (loggedInUser) {
      setUser(JSON.parse(loggedInUser))
    }
  }, [])

  return (
    <ThemeProvider theme={darkTheme}>
      {user
        ? (<ThemeProvider theme={lightTheme}>
          <Home setUser={setUser} user={user} />
        </ThemeProvider>)
        : (<Login setUser={setUser} />)
      }
    </ThemeProvider >
  )
}

export default App