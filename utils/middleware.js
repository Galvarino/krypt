const errorHandler = (err, req, res, nxt) => {
    console.log(err.message)
    nxt(err)
}

module.exports = { errorHandler }